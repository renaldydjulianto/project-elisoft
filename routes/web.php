<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ApiUserController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\ProdukController;
use App\Http\Controllers\HomeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [HomeController::class, 'index']);
// Route::get('/', function () {
//     return view('auth.n_login');
//     // return view('layouts.template');
//     // return view('auth.login');
// });

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    
    //dashboard
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    
    //USER
    Route::get('/user',[UserController::class,'index'])->name('admin.user.index');
    Route::get('/user/create',[UserController::class,'create'])->name('admin.user.create');
    Route::post('/user/create/store',[UserController::class,'store'])->name('admin.user.store');
    Route::get('/user/show/{id}',[UserController::class,'show'])->name('admin.user.show');
    Route::get('/user/edit/{id}',[UserController::class,'edit'])->name('admin.user.edit');
    Route::post('/user/update/{id}',[UserController::class,'update'])->name('admin.user.update');
    Route::delete('/user/delete/{id}',[UserController::class,'destroy'])->name('admin.user.destroy');
    
    //Produk
    Route::get('/produk',[ProdukController::class,'index'])->name('admin.produk.index');
    Route::get('/produk/create',[ProdukController::class,'create'])->name('admin.produk.create');
    Route::get('/produk/create/store',[ProdukController::class,'store'])->name('admin.produk.store');
    Route::get('/produk/show/{id}',[ProdukController::class,'show'])->name('admin.produk.show');
    Route::get('/produk/edit/{id}',[ProdukController::class,'edit'])->name('admin.produk.edit');
    Route::get('/produk/update/{id}',[ProdukController::class,'update'])->name('admin.produk.update');
    Route::delete('/produk/delete/{id}',[ProdukController::class,'destroy'])->name('admin.produk.destroy');

});
Route::resource('/api/user', ApiUserController::class);
// Route::resource('/api/produk', ProdukController::class);