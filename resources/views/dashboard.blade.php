@extends('layouts.template')
@section('title','Dashboard')
    
@section('content')
<!-- Small boxes (Stat box) -->

  <div class="row">
    <div class="col-lg-4 col-6">
      <!-- small box -->
      <div class="small-box bg-warning">
        <div class="inner">
          <h3>{{ count($users)}}</h3>

          <p>Users</p>
        </div>
        <div class="icon">
          <i class="fas fa-user-tie"></i>
        </div>
      </div>
    </div>
  </div>
<!-- /.row -->
@stop