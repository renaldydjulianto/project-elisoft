<footer class="main-footer">
    <strong>Copyright &copy; <?php echo date("Y"); ?>  <a href="">Renaldy Dwi Julianto</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.1
    </div>
  </footer>