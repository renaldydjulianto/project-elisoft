@extends('layouts.template')
@section('title','Data Produk')
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('template') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush
@section('content')
<div class="row">
	<div class="col">
		<div class="card">
			<div class="card-header">
				<a href="" class="btn btn-primary btn-sm">+ Tambah Produk</a>
			</div>
			<div class="card-body table-responsive">
				<table id="dataTable1" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Nickname</th>
                            <th>Action</th>
                        </tr>
                    </thead>
				<tbody>
				{{-- @php 
					$no=1;
				@endphp --}}

				@foreach($users as $user)
				<tr>
				  <td>{{ $user['id'] }}</td>
				  <td>{{ $user['name'] }}</td>
				  <td>{{ $user['nickname'] }}</td>
				  <td>
					<div class="row ml-2">
						<a href="" class="btn btn-primary btn-sm"><i class="fas fa-eye fa-fw"></i></a>
						&nbsp;
						&nbsp;
						<a href="" class="btn btn-primary btn-sm"><i class="fas fa-edit fa-fw"></i></a>
						<form method="POST" action="">
							@csrf
							@method('DELETE')
							<button onclick="return confirm('Yakin hapus ?')" type="submit" class="btn btn-danger btn-sm ml-2"><i class="fas fa-trash fa-fw"></i></button>
						</form>
					</div>
				</td>
				</tr>
				@endforeach
				</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop
@push('js')
<!-- DataTables -->
<script src="{{ asset('template') }}/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{ asset('template') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#dataTable1").DataTable();
  });
</script>
@endpush