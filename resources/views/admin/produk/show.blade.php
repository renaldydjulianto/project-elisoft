@extends('layouts.template')
@section('title','Detail User')
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('template') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush
@section('content')
<div class="row">
	<div class="col">
		<div class="card">
			<div class="card-header">
				<a href="{{ route('admin.produk.index') }}" class="btn btn-success btn-sm">Kembali</a>
			</div>
			<div class="card-body ">
                <form method="" action="">
					@csrf
					<div class="form-group">
						<label for="name">Name</label>
						<input value="{{ $user['name'] }}" class="form-control" type="" name="name" id="name">
					</div>
					<div class="form-group">
						<label for="nickname">Nickname</label>
						<input value="{{ $user['nickname'] }}" required="" class="form-control" type="" name="nickname" id="nickname" placeholder="">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop
@push('js')
<!-- DataTables -->
<script src="{{ asset('template') }}/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{ asset('template') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#dataTable1").DataTable();
  });
</script>
@endpush