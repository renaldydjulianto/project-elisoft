@extends('layouts.template')
@section('title','Create Produk')
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('template') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush
@section('content')
<div class="row">
	<div class="col">
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert- info') }}">
            {{ Session::get('message') }}
            <a class="close" aria-hidden="true" data- dismiss="alert">x</a>
            </p>
        @endif
		<div class="card">
			<div class="card-header">
				<a href="{{ route('admin.produk.index') }}" class="btn btn-success btn-sm">Kembali</a>
			</div>
			<div class="card-body ">
                <form method="POST" action="{{ route('admin.produk.store') }}">
					@csrf
					<div class="form-group">
						<label for="name">Name</label>
						<input class="form-control" type="" name="name" id="name" placeholder="" required>
						@error('name')
                        	<div class="invalid-feedback text-danger">{{ $message}} </div>
                    	@enderror
					</div>
					<div class="form-group">
						<label for="nickname">Nickname</label>
						<input class="form-control @error('nickname') is-invalid @enderror" type="" name="nickname" id="nickname" placeholder="" required>
						@error('nickname')
                        	<div class="invalid-feedback text-danger">{{ $message}} </div>
                    	@enderror
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-sm">SIMPAN</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop
@push('js')
<!-- DataTables -->
<script src="{{ asset('template') }}/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{ asset('template') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#dataTable1").DataTable();
  });
</script>
@endpush