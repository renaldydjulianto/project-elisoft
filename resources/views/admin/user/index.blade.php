@extends('layouts.template')
@section('title','Data User')
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('template') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush
@section('content')
<div class="row">
	<div class="col">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
            </div>
        @endif
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert- info') }}">
            {{ Session::get('message') }}
            <a class="close" aria-hidden="true" data- dismiss="alert">x</a>
            </p>
        @endif
		<div class="card">
			<div class="card-header">
				<a href="{{ route('admin.user.create') }}" class="btn btn-primary btn-sm">+ Tambah User</a>
			</div>
			<div class="card-body table-responsive">
				<table id="dataTable1" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Last Seen</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
				<tbody>
				@php 
					$no=1;
				@endphp

				@foreach($users as $user)
				<tr>
				  <td>{{ $no++ }}</td>
				  <td>{{ $user->name }}</td>
				  <td>{{ $user->email }}</td>
				  <td>{{ Carbon\Carbon::parse($user->last_seen)->diffForHumans() }}</td>
				  <td>
                      @if(Cache::has('user-is-online-' . $user->id))
                            <span class="text-success">Online</span>
                      @else
                            <span class="text-secondary">Offline</span>
                      @endif
                  </td>
				  <td>
				  	<div class="row ml-2">
				  		<a href="{{ route('admin.user.show',$user->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-eye fa-fw"></i></a>
                          &nbsp;
                          &nbsp;
				  		<a href="{{ route('admin.user.edit',$user->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-edit fa-fw"></i></a>
				  		<form method="POST" action="{{ route('admin.user.destroy',$user->id) }}">
				  			@csrf
				  			@method('DELETE')
				  			<button onclick="return confirm('Yakin hapus ?')" type="submit" class="btn btn-danger btn-sm ml-2"><i class="fas fa-trash fa-fw"></i></button>
				  		</form>
				  	</div>
				  </td>
				</tr>
				@endforeach
				</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop
@push('js')
<!-- DataTables -->
<script src="{{ asset('template') }}/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{ asset('template') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#dataTable1").DataTable();
  });
</script>
@endpush