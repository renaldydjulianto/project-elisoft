@extends('layouts.template')
@section('title','Detail User')
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('template') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush
@section('content')
<div class="row">
	<div class="col">
		<div class="card">
			<div class="card-header">
				<a href="{{ route('admin.user.index') }}" class="btn btn-success btn-sm">Kembali</a>
			</div>
			<div class="card-body ">
                <form method="" action="">
					@csrf
					<div class="form-group">
						<label for="name">Name</label>
						<input value="{{ $user->name }}" class="form-control" type="" name="name" id="name">
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<input value="{{ $user->email }}" required="" class="form-control" type="" name="email" id="email" placeholder="">
					</div>
					<div class="form-group">
						<label for="tanggal_lahir">Tanggal Lahir</label>
						<input required class="form-control" type="date" name="tanggal_lahir" id="tanggal_lahir" placeholder="">
					</div>
					<div class="form-group">
						<label for="jenis_kelamin">Jenis Kelamin</label>
						<input value="{{ $user->jenis_kelamin }}" class="form-control" type="" name="jenis_kelamin" id="jenis_kelamin">
					</div>
					<div class="form-group">
						<label for="agama">Agama</label>
						<input value="{{ $user->agama }}" class="form-control" type="" name="agama" id="agama">
					</div>
					<div class="form-group">
						<label for="nomor_telepon">No. Telepon</label>
						<input value="{{ $user->nomor_telepon }}" class="form-control" type="" name="nomor_telepon" id="nomor_telepon">
					</div>
					<div class="form-group">
						<label for="alamat">Alamat</label>
						<input value="{{ $user->alamat }}" class="form-control" type="" name="alamat" id="alamat">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop
@push('js')
<!-- DataTables -->
<script src="{{ asset('template') }}/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{ asset('template') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#dataTable1").DataTable();
  });
</script>
@endpush