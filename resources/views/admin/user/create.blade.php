@extends('layouts.template')
@section('title','Create User')
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('template') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush
@section('content')
<div class="row">
	<div class="col">
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert- info') }}">
            {{ Session::get('message') }}
            <a class="close" aria-hidden="true" data- dismiss="alert">x</a>
            </p>
        @endif
		<div class="card">
			<div class="card-header">
				<a href="{{ route('admin.user.index') }}" class="btn btn-success btn-sm">Kembali</a>
			</div>
			<div class="card-body ">
                <form method="POST" action="{{ route('admin.user.store') }}">
					@csrf
					<div class="form-group">
						<label for="name">Name</label>
						<input class="form-control" type="" name="name" id="name" placeholder="" required>
						@error('name')
                        	<div class="invalid-feedback text-danger">{{ $message}} </div>
                    	@enderror
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<input class="form-control @error('email') is-invalid @enderror" type="" name="email" id="email" placeholder="" required>
						@error('email')
                        	<div class="invalid-feedback text-danger">{{ $message}} </div>
                    	@enderror
					</div>
					<div class="form-group">
						<label for="tanggal_lahir">Tanggal Lahir</label>
						<input required class="form-control" type="date" name="tanggal_lahir" id="tanggal_lahir" placeholder="">
					</div>
					<div class="form-group">
						<label for="jenis_kelamin">Jenis Kelamin</label>
						<select name="jenis_kelamin" id="jenis_kelamin" class="form-control" required>
							<option value="">-- Pilih --</option>
							<option value="L">Laki laki</option>
							<option value="P">Perempuan</option>
						</select>
					</div>
					<div class="form-group">
						<label for="agama">Agama</label>
						<select name="agama" id="agama" class="form-control" required>
							<option value="">-- Pilih --</option>
							<option value="islam">Islam</option>
							<option value="protestan">Protestan</option>
							<option value="katolik">Katolik</option>
							<option value="hindu">Hindu</option>
							<option value="buddha">Buddha</option>
							<option value="khonghucu">Khonghucu</option>
						</select>
					</div>
					<div class="form-group">
						<label for="nomor_telepon">No. Telepon</label>
						<input required class="form-control" type="number" name="nomor_telepon" id="nomor_telepon" placeholder="">
					</div>
					<div class="form-group">
						<label for="alamat">Alamat</label>
						<textarea required class="form-control" type="text" name="alamat" id="alamat" placeholder=""></textarea>
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input required="" class="form-control" type="password" name="password" id="password" placeholder="">
					</div>
					<div class="form-group">
						<label for="password_confirmation">Password Confirmation</label>
						<input required="" class="form-control" type="password" name="password_confirmation" id="password_confirmation" placeholder="">
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-sm">SIMPAN</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop
@push('js')
<!-- DataTables -->
<script src="{{ asset('template') }}/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{ asset('template') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#dataTable1").DataTable();
  });
</script>
@endpush