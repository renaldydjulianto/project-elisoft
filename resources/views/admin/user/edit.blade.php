@extends('layouts.template')
@section('title','Edit User')
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('template') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush
@section('content')
<div class="row">
	<div class="col">
		<div class="card">
			<div class="card-header">
				<a href="{{ route('admin.user.index') }}" class="btn btn-success btn-sm">Kembali</a>
			</div>
			<div class="card-body ">
                <form method="POST" action="{{ route('admin.user.update',$user->id) }}">
					@csrf
                    @method('POST')
					<div class="form-group">
						<label for="name">Name</label>
						<input value="{{ $user->name }}" class="form-control" type="" name="name" id="name">
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<input value="{{ $user->email }}" required="" class="form-control" type="" name="email" id="email" placeholder="">
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input value="{{ $user->name }}" required="" class="form-control" type="hidden" name="old_password">
                        <input class="form-control" type="password" name="password" id="password" placeholder="">
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-sm">SIMPAN</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop
@push('js')
<!-- DataTables -->
<script src="{{ asset('template') }}/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{ asset('template') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#dataTable1").DataTable();
  });
</script>
@endpush